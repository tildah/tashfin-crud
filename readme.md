# Tashfin CRUD
A library for TashfinJS that returns the main crud functions using `MongoDB` as DB and `Tatabot` as validator.

## Arguments
Tashfin CRUD takes 4 arguments:
* `moduleName`: String serving as collection name
* `schema`: Schema to use in validation
* `db`: MongoDB instance
* `moduleOptions`: An object containing these options:
    * `listQuery`: The MongoDB aggregation to use for `GET_$root`.
    * `paranoid`: Boolean. If true `DELETE_root` won't completely delete item. Instead it will set its `trash` property to `true`.

## Return
Tashfin CRUD return 5 functions:
* `GET_$root`
* `GET_root`
* `POST_root`
* `PUT_root`
* `DELETE_root`

## Installation
```
npm i -save tashfin-crud
```

## Examples
### Raw use
The simplest example:

First start by instantiate db and pass it to module in your **main app file**:
```javascript
const { MongoClient } = require("mongodb");
const { listen } = require("tashfin");
const users = require("./app_modules/users");

mongoClient.connect(<DB_URL>, async (err, db) => {
  if (err) throw err;
  console.log("Connected successfully to database");

  const logRequest = true;
  const modules =  { users: users(db) };

  listen({modules, logRequest});
});
```

Then in **your module file**:
```javascript
const tashfinCRUD = require("tashfin-crud");

// The schema is set in the file for example purpose, but it's better to put it in another 
// file, import it.
const schema = {
  "*name": "string",
  email: "email",
  age: { type: "integer", min: 13 },
  role: { type: "enum", values: ["admin", "follower"] }
}

module.exports = db => tashfinCRUD("users", schema, db);
```

Now, you should be able to use these routes:
* `GET    /users`
* `GET    /users/123`
* `POST   /users`
* `PUT    /users/123`
* `DELETE /users/123`

### Custom Aggregation
If you want to set an aggregation for `GET_$root`, here is how you do:

**your module file**:
```javascript
const tashfinCRUD = require("tashfin-crud");

// The schema is set in the file for example purpose, but it's better to put it in another 
// file, import it.
const schema = {
  "*name": "string",
  email: "email",
  age: { type: "integer", min: 13 },
  role: { type: "enum", values: ["admin", "follower"] }
}

const listQuery = [ //...My custom aggregation ];

module.exports = db => tashfinCRUD("users", schema, db, { listQuery });
```

### Custom Functions
Since Tashfin CRUD just returns functions, you can return custom functions instead of the returned ones from the library.

Here is an example of custom `PUT_root`:
```javascript
const tashfinCRUD = require("tashfin-crud");

// The schema is set in the file for example purpose, but it's better to put it in another 
// file, import it.
const schema = {
  "*name": "string",
  email: "email",
  age: { type: "integer", min: 13 },
  role: { type: "enum", values: ["admin", "follower"] }
}

const listQuery = [ //...My custom aggregation ];

module.exports = db => {

  const tashfinCRUDFunctions = tashfinCRUD("users", schema, db, listQuery);

  const DELETE_root = () => { statusCode: 403, content: "Here, we don't delete things" };

  return { ...tashfinCRUDFunctions, DELETE_root};
}
```
