const { ObjectId } = require("mongodb");
const { parseBody, resJSON } = require("tashfin");
const tatabot = require("tatabot");
const { validate } = tatabot;

tatabot.addType("mongoid", {
  coerce: value => new ObjectId(value),
  validate: value => {
    const re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i
    return re.test(value)
  }
})

module.exports = (moduleName, schema, db, moduleOptions = {}) => {

  const defaultListQuery = () => [{$match: {trash: {$ne: true}}}];

  const {
    paranoid,
    listQuery = defaultListQuery,
    customTypes = []
  } = moduleOptions;
  
  customTypes.forEach(({name, coerce, validate}) => {
    tatabot.addType(name, { coerce, validate })
  })

  const GET_$root = async req => {
    const result = await db.collection(moduleName).aggregate(listQuery(req));
    return resJSON(result);
  }

  const GET_root = async (req, id) => {
    const filter = { _id: new ObjectId(id), trash: { $ne: true } };
    const item = await db.collection(moduleName).findOne(filter);
    return resJSON(item);
  }

  const POST_root = async req => {
    const body = await parseBody(req);
    const {isValid, errors, coersion} = validate(body, schema);
    if(!isValid) return { statusCode: 400, ...resJSON(errors) };
    const insertResult = await db.collection(moduleName).insertOne(coersion);
    return resJSON(insertResult.ops[0]);
  }

  const updateOne = async (id, body) => {
    const filter = { _id: new ObjectId(id) };
    const update = { $set: body };
    const result = await db.collection(moduleName).updateOne(filter, update);
    return resJSON(result.op[0]);
  }

  const PUT_root = async (req, id) => {
    const {_id, ...body} = await parseBody(req);
    const settings = { noRequired: true };
    const {isValid, errors, coersion} = validate(body, schema, settings);
    if(!isValid) return { statusCode: 400, ...resJSON(errors) };
    return await updateOne(id, body);
  }

  const paranoidDelete = id => updateOne(id, { trash: true });

  const DELETE_root = async (req, id) => {
    if(paranoid) return paranoidDelete(id);
    await db.collection(moduleName).deleteOne({_id: new ObjectId(id)});
    return { statusCode: 204 }
  }

  return { GET_$root, GET_root, POST_root, PUT_root, DELETE_root }
}
